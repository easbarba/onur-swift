/*
* Onur is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Onur is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Onur. If not, see <https://www.gnu.org/licenses/>.
**/

import Foundation

class Files {
    func names() -> Array<String> {
        let globals = Globals()
        let fileManager = FileManager.default
        let enumerator = fileManager.enumerator(atPath: globals.onurHome.path)

        var result : [String] = []
        while let element = enumerator?.nextObject() as? String {
            if let fType = enumerator?.fileAttributes?[FileAttributeKey.type] as? FileAttributeType{
                if fType.rawValue != "NSFileTypeDirectory"  { // can be either files or links
                    let elmFullPath = globals.onurHome.appendingPathComponent(element)
                    if !element.hasSuffix(".json") {
                        continue
                    }

                    do {
                        let elmRealPath = try fileManager.destinationOfSymbolicLink(atPath: elmFullPath.path)
                        if !fileManager.fileExists(atPath: elmRealPath) {
                            continue
                        }

                        result.append(element)

                    } catch let error as NSError {
                        switch error.code {
                        case NSFileReadNoSuchFileError:
                            print("Error: The symbolic link does not exist at \(elmFullPath)")
                        case NSFileReadInvalidFileNameError:
                            print("Error: Path is not a symbolic link")
                        case NSFileReadNoPermissionError:
                            print("Error: No permission to read the symbolic link")
                        default:
                            print("Unknown error: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }

        return result
    }
}
