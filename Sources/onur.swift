/*
* Onur is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Onur is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Onur. If not, see <https://www.gnu.org/licenses/>.
**/

import ArgumentParser

@main
struct Cli: ParsableCommand {
  static let configuration = CommandConfiguration(
    abstract: "Easily manage multiple FLOSS repositories.",
    version: "0.1.0",
    subcommands: [Grab.self, Archive.self, Config.self],
    defaultSubcommand: Grab.self)
}

extension Cli {
  struct Grab: ParsableCommand {
      static let configuration = CommandConfiguration(abstract: "grab all projects", aliases: ["g"])

      mutating func run() {
          Files().names().forEach { x in
              print(x)
          }
      }
  }

  struct Archive: ParsableCommand {
      static let configuration = CommandConfiguration(abstract: "backing up projects", aliases: ["a"])

      @Argument(help: "Projects to back up")
      var projects: String

      mutating func run() {
          print("backing up: \(projects)")
      }
  }

  struct Config: ParsableCommand {
      static let configuration = CommandConfiguration(commandName: "config",
                                                      abstract: "manage configuration",
                                                      usage: "LANG.TOPIC NAME URL BRANCH",
                                                      version: "0.1.0",
                                                      aliases: ["c"])

      @Argument(help: "Configs details")
      var options: [String]

      mutating func run() {
          print("configuration: \(options)")

      }
  }

}
