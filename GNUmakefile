# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

# DEPS: gcc meson ninja muon coreutils

.DEFAULT_GOAL: test
NAME := onur
PREFIX     ?= ${HOME}/.local/bin

# ============================ LOCAL

.PHONY: all
all: local.all

.PHONY: local.all
local.all:
	swift build --configuration release --verbose

.PHONY: local.test
local.test:
	swift test -v

.PHONY: local.clean
local.clean:
	rm -rf ./.build

.PHONY: local.install
local.install:
	cp -v .build/debug/onur ${PREFIX}/onur-swift

.PHONY: local.uninstall
local.uninstall:
	rm ${PREFIX}/onur-swift

.PHONY: local.lint
local.lint:
	swift format lint */**/*.swift

.PHONY: local.format
local.format:
	swift format -i */**/*.swift

.PHONY: local.usage
local.usage:
	swift run --quiet onur --help

.PHONY: local.grab
local.grab:
	swift run --quiet onur grab

.PHONY: local.archive
local.archive:
	swift run --quiet onur archive awesomewm,guile,git

.PHONY: local.config
local.config:
	swift run --quiet onur config c.linux cryptsetup https://git.kernel.org/pub/scm/utils/cryptsetup/cryptsetup.git master

.PHONY: local.test
